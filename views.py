import datetime
import json
import ast
import requests
import hashlib
import re, sys, os
import dateutil.parser
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.template.context_processors import csrf
from django.core.serializers.json import DjangoJSONEncoder
from django.conf import settings
from django.utils import timezone
from django.utils.encoding import force_unicode
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse
from django.http.response import Http404
from django.template import RequestContext, loader
from operator import itemgetter
from itertools import groupby
from transliterate import translit
from .models import *

def index(request):
    template = loader.get_template('qiwi/index.html')
    
    items = Qiwimsg.objects.all().order_by('id')[:10]
    
    api_access_token = 'b20b8ade7fe36662fa6611556476ae72'

    my_login = '+79252757855'

    session = requests.Session()
    session.headers['authorization'] = 'Bearer ' + api_access_token
    parameters = {'rows': '50'}
    request = session.get('https://edge.qiwi.com/payment-history/v1/persons/' + my_login + '/payments', params=parameters)
    response = json.loads(request.text)
    lastdate = dateutil.parser.parse(response['data'][0]['date'].split('+')[0])
    
    
    context_data = {
	    'lastdate' : lastdate,
	    'items' : items,
	    'json' : response['data'][:1],
    }
    
    context = RequestContext(request, context_data)
    return HttpResponse(template.render(context))