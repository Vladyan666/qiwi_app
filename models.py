# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import json
from django.db import models
from django.utils import timezone
from django import forms
from django.contrib.postgres.fields import JSONField
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User, Group


# Create your models here.
class Page(models.Model):
    class Meta:
        abstract = True
        
    seo_h1 = models.CharField( max_length=200 , verbose_name="H1" , null=True , blank=True )
    seo_title = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    seo_description = models.CharField( max_length=500 , verbose_name="Description" , null=True , blank=True )
    seo_keywords = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    alias = models.SlugField( max_length=200 , verbose_name=u"Псевдоним", null=True , blank=True)
    menutitle = models.CharField( max_length=200 , verbose_name=u"Название в меню" , null=True , blank=True )
    menushow = models.BooleanField( default=True, verbose_name=u"Показывать в меню" )
    menuposition = models.IntegerField( verbose_name=u"Позиция в меню", null=True , blank=True )
    sitemap = models.BooleanField( default=True , verbose_name=u"Показывать в карте сайта" )
    content = models.TextField( verbose_name=u"Статья" , null=True , blank=True )

            
class TextPage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    
    def __unicode__(self):
        return self.name
        
class Qiwimsg(Page):
    class Meta:
        verbose_name = u"Киви платеж"
        verbose_name_plural = u"Киви платежи"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    who = models.CharField( max_length=200 , verbose_name=u"Имя отправителя" )
    how = models.CharField( max_length=200 , verbose_name=u"Сумма" )
    phone = models.ForeignKey('Token', related_name='qiwi_messages', verbose_name=u'телефон')
    comment = models.CharField( max_length=200 , verbose_name=u"Комментарий",null=True , blank=True )
    datetime = models.DateTimeField(verbose_name=u'Дата платежа',null=True , blank=True)
    
    def __unicode__(self):
        return self.name
        
class Token(Page):
    class Meta:
        verbose_name = u"Киви токен"
        verbose_name_plural = u"Киви токены"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название" )
    token = models.CharField( max_length=200 , verbose_name=u"Токен QIWI" )
    bot_token = models.CharField( max_length=200 , verbose_name=u"Токен Бота телеграмм", null=True, blank=True )
    chat_id = models.CharField( max_length=200 , verbose_name=u"ID чата телеграмм для пуша", null=True, blank=True )
    phone = models.CharField( max_length=200 , verbose_name=u"Телефон в формате +79996661366" )
    comment = models.CharField( max_length=200 , verbose_name=u"Комментарий",null=True , blank=True )
    
    def __unicode__(self):
        return self.name
        
class Telegram(Page):
    class Meta:
        verbose_name = u"Телеграм канал"
        verbose_name_plural = u"Телеграм каналы"
        
    name = models.CharField( max_length=200 , verbose_name=u"Название чата" )
    chat_id = models.CharField( max_length=200 , verbose_name=u"ID чата" )
    qiwi = models.ForeignKey(Token, verbose_name=u"Отслеживаемый кошелёк" )
    comment = models.CharField( max_length=200 , verbose_name=u"Комментарий",null=True , blank=True )
    
    def __unicode__(self):
        return self.name