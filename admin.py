# -*- coding: utf-8 -*-
from django.contrib import admin

from qiwiapp.models import TextPage, Qiwimsg, Token, Telegram

# Register your models here.
page_fields = [
    (u"Настройки страницы" , {'fields':['menutitle','alias','menushow','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','content',]})
]
page_list = ('menushow','sitemap')

#Страницы
class TextPageAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ("name",)}
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition',]}),] + page_fields
    
admin.site.register(TextPage, TextPageAdmin)

#Сообщения
class QiwimsgAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ('name',)}
    list_display = ('name', 'datetime', 'who', 'how','comment')
    fieldsets = [(u'Основные',  {'fields':['name', 'datetime', 'who', 'how','comment', 'menuposition',]}),] + page_fields

admin.site.register(Qiwimsg, QiwimsgAdmin)

#Токены
class TokenAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ('name',)}
    list_display = ('name', 'token', 'phone', 'comment')
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition', 'bot_token', 'chat_id', 'token', 'phone', 'comment']}),] + page_fields

admin.site.register(Token, TokenAdmin)

#Телеги
class TelegramAdmin(admin.ModelAdmin):
    prepopulated_fields = {"alias": ('name',)}
    list_display = ('name', 'chat_id', 'qiwi', 'comment')
    fieldsets = [(u'Основные',  {'fields':['name', 'menuposition', 'chat_id', 'qiwi', 'comment']}),] + page_fields

admin.site.register(Telegram, TelegramAdmin)