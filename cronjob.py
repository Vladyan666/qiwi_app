# -*- coding: utf8 -*-

import os
import sys
import re, requests, json
import dateutil.parser
import django
from django.apps import apps

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seosaf.settings")
django.setup()

from qiwiapp.models import *

for u_token in Token.objects.all():
    print(', '.join([u_token.token, u_token.phone, u_token.chat_id, u_token.bot_token]))
    api_access_token = u_token.token
    my_login = u_token.phone
    session = requests.Session()
    session.headers['authorization'] = 'Bearer ' + api_access_token
    parameters = {'rows': '10'}
    request = session.get('https://edge.qiwi.com/payment-history/v1/persons/' + my_login + '/payments', params=parameters)
    response = json.loads(request.text)
    for i in response['data']:
        if i['type'] == 'IN':
            formated_date = dateutil.parser.parse(i['date'].split('+')[0])
            obj, created = Qiwimsg.objects.get_or_create(datetime=formated_date ,how=i['sum']['amount'], who=i['account'], phone=Token.objects.get(phone=my_login), defaults={'name': u'123', 'comment': i['comment']})
            if created:
                print(obj.datetime.strftime("%Y-%m-%d %H:%M:%S"))
                requests.get(u'https://api.telegram.org/bot' + u_token.bot_token.encode('utf8') + u'/sendMessage?chat_id=-' + u_token.chat_id.encode('utf8') + u'&text=Поступил перевод от ' + obj.who + u', сумма - ' + str(obj.how) + u', коментарий: ' + (obj.comment if obj.comment else '') + u'\r\nВремя зачисления: ' + obj.datetime.strftime("%Y-%m-%d %H:%M:%S"))